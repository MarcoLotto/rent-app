package utils;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class TokenUtils {

    public static final String X_AUTH_EXTERNAL_CLIENT_PARAM = "X-Auth-External-Client";
    public static final String X_AUTH_EXTERNAL_TOKEN_PARAM = "X-Auth-External-Token";
    public static final String X_AUTH_TOKEN_PARAM = "X-Auth-Token";
    public static final String X_AUTH_USERNAME = "X-Auth-Username";
    public static final String X_AUTH_PASSWORD = "X-Auth-Password";


    public static void saveToken(Context ctx, String token) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(TokenUtils.X_AUTH_TOKEN_PARAM, token);
        editor.commit();
    }

    public static String getToken(Context ctx){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ctx);
        return preferences.getString(TokenUtils.X_AUTH_TOKEN_PARAM, null);
    }
}
