package REST;
import android.content.Context;

import com.loopj.android.http.*;

import java.util.Map;

import cz.msebera.android.httpclient.HttpEntity;
import utils.TokenUtils;


public class HttpUtils {
    //private static final String BASE_URL = "http://192.168.0.108:8092/"; // Local Marco
    private static final String BASE_URL = "http://ec2-54-227-102-108.compute-1.amazonaws.com:8092/"; // Contra servidor de desarrollo


    private static final String CONTENT_TYPE_APP_JSON = "application/json";
    private static final int REQUESTS_TIMEOUT_IN_MILLISECONDS = 7000;


    public static void get(Context ctx, String url, RequestParams params, AppJsonHttpResponseHandler responseHandler) {
        getHttpClient(ctx).get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(Context ctx, String url, HttpEntity entity, AppJsonHttpResponseHandler responseHandler) {
        getHttpClient(ctx).post(ctx, getAbsoluteUrl(url), entity, CONTENT_TYPE_APP_JSON, responseHandler);
    }

    public static void post(Context ctx, String url, HttpEntity entity, Map<String, String> headers, AppJsonHttpResponseHandler responseHandler) {
        getHttpClient(ctx, headers).post(ctx, getAbsoluteUrl(url), entity, CONTENT_TYPE_APP_JSON, responseHandler);
    }

    public static void getByUrl(Context ctx, String url, RequestParams params, AppJsonHttpResponseHandler responseHandler) {
        getHttpClient(ctx).get(url, params, responseHandler);
    }

    public static void postByUrl(Context ctx, String url, RequestParams params, AppJsonHttpResponseHandler responseHandler) {
        getHttpClient(ctx).post(url, params, responseHandler);
    }

    private static AsyncHttpClient getHttpClient(Context ctx, Map<String, String> headers){
        AsyncHttpClient client = getHttpClient(ctx);
        for(String headerName : headers.keySet()){
            client.addHeader(headerName, headers.get(headerName));
        }
        return client;
    }

    private static AsyncHttpClient getHttpClient(Context ctx){
        AsyncHttpClient client = new AsyncHttpClient();
        client.setTimeout(REQUESTS_TIMEOUT_IN_MILLISECONDS);
        client.setConnectTimeout(REQUESTS_TIMEOUT_IN_MILLISECONDS);
        client.addHeader(TokenUtils.X_AUTH_TOKEN_PARAM, TokenUtils.getToken(ctx));
        return client;
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
}