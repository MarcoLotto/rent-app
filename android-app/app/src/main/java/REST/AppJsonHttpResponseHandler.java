package REST;

import android.content.Context;
import android.content.Intent;

import com.company.rentApp.MainLoginActivity;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpStatus;
import utils.TokenUtils;

/**
 * Created by marco on 01/09/17.
 */

public abstract class AppJsonHttpResponseHandler extends JsonHttpResponseHandler {

    private static final String LOG_TAG = "AppJsonHttpResponseHandler";
    private Context ctx;

    public AppJsonHttpResponseHandler(Context ctx){
        super();
        this.ctx = ctx;
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
        this.handleUnauthorizedRequests(statusCode);
        this.onError(statusCode, headers, throwable, errorResponse);
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
        this.handleUnauthorizedRequests(statusCode);
        JSONObject response = new JSONObject();
        try {
            response.put("error", errorResponse);
        }
        catch (JSONException e) {
            e.printStackTrace();
        } finally {
            this.onError(statusCode, headers, throwable, response);
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
        this.handleUnauthorizedRequests(statusCode);
        JSONObject response = new JSONObject();
        try {
            response.put("error", responseString);
        }
        catch (JSONException e) {
            e.printStackTrace();
        } finally {
            this.onError(statusCode, headers, throwable, response);
        }
    }

    public void onError(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
        AsyncHttpClient.log.w(LOG_TAG, "onError(int, Header[], Throwable, JSONObject) was not overriden, but callback was received", throwable);
    }

    public void handleUnauthorizedRequests(int statusCode) {
        if(statusCode == HttpStatus.SC_UNAUTHORIZED){
            // El token no sirve mas, lo limpiamos
            TokenUtils.saveToken(this.ctx, null);

            // Redirigimos a la pantalla de login
            Intent intent = new Intent(this.ctx, MainLoginActivity.class);
            this.ctx.startActivity(intent);
        }
    }

    public Context getContext(){
        return this.ctx;
    }
}
