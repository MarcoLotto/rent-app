package com.company.rentApp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import REST.AppJsonHttpResponseHandler;
import REST.HttpUtils;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import utils.TokenUtils;

public class MainLoginActivity extends AppCompatActivity {

    private static final String FACEBOOK_CLIENT = "facebook";

    // Facebook login
    CallbackManager callbackManager;
    private LoginButton facebookLoginButton;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_login);

        // Obtenemos el token del local storage si es que existe
        this.evaluateAlreadyExistantLogin();

        // No tenemos token, invalidamos la sesion de facebook y comenzamos el login
        this.invalidateFacebookToken();
        this.buildFacebookLogin();
    }

    public void goToLocalAccountLoginActivity(View view)
    {
        Intent intent = new Intent(this, LocalAccountLoginActivity.class);
        startActivity(intent);
    }

    private void invalidateFacebookToken() {
        LoginManager.getInstance().logOut();
    }

    private void evaluateAlreadyExistantLogin() {
        // Si ya tenemos un token, no pasamos por las pantallas de login
        if(TokenUtils.getToken(this.getApplicationContext()) != null) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    private void buildFacebookLogin() {
        this.callbackManager = CallbackManager.Factory.create();
        this.facebookLoginButton = (LoginButton) findViewById(R.id.facebook_login_button);
        this.facebookLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // Enviamos el token obtenido al login del server
                try {
                    loginOnServerWithFacebookToken(loginResult.getAccessToken().getToken());
                } catch (UnsupportedEncodingException e) {
                    Log.d("Response", e.toString());
                }
            }

            @Override
            public void onCancel() {}

            @Override
            public void onError(FacebookException e) {
                Log.e("FACEBOOK_LOGIN_ERROR", e.getMessage());
            }
        });
    }

    private void loginOnServerWithFacebookToken(String token) throws UnsupportedEncodingException {
        progressDialog = ProgressDialog.show(this, "Logueando al servidor", "Por favor espere...");
        Map<String, String> headers = this.buildHeadersToServerLogin(FACEBOOK_CLIENT, token);
        HttpUtils.post(this.getApplicationContext(),"/api/v1/login", new StringEntity(""), headers, new AppJsonHttpResponseHandler(this){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("Response", response.toString());
                for(Header header : headers){
                    if(header.getName().equals(TokenUtils.X_AUTH_TOKEN_PARAM)){
                        saveLoginToken(header.getValue());
                    }
                }
                progressDialog.hide();
                evaluateAlreadyExistantLogin();
            }
            @Override
            public void onError(int statusCode, Header[] headers, Throwable throwable, JSONObject error) {
                Log.d("Error: ", throwable.toString() + " - Error - " + error);
                progressDialog.hide();
            }
        });
    }

    private void saveLoginToken(String token){
        TokenUtils.saveToken(this.getApplicationContext(), token);
    }

    private Map<String,String> buildHeadersToServerLogin(String client, String token) {
        Map<String, String> headers = new HashMap<>();
        headers.put(TokenUtils.X_AUTH_EXTERNAL_CLIENT_PARAM, client);
        headers.put(TokenUtils.X_AUTH_EXTERNAL_TOKEN_PARAM, token);
        return headers;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
