package com.company.rentApp;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import REST.AppJsonHttpResponseHandler;
import REST.HttpUtils;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import utils.AlertUtils;
import utils.TokenUtils;

public class LocalAccountRegistryActivity extends AppCompatActivity {

    private static final String REGISTRY_ERROR_TITLE = "Error en el registro";
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_account_registry);
    }

    // Es llamado desde el layout
    public void onRegistryButtonSelected(View view){
        String username = ((EditText) findViewById(R.id.usernameInput)).getText().toString();
        String name = ((EditText) findViewById(R.id.nameInput)).getText().toString();
        String surename = ((EditText) findViewById(R.id.surenameInput)).getText().toString();
        String password = ((EditText) findViewById(R.id.passwordInput)).getText().toString();

        if(!username.isEmpty() && !name.isEmpty() && !surename.isEmpty() && !password.isEmpty()){
            this.makeRegistryRequest(username, password, name, surename);
        }
    }

    private void makeRegistryRequest(String username, String password, String name, String surename) {
        try {
            Map<String, String> headers = new HashMap<>();
            headers.put(TokenUtils.X_AUTH_USERNAME, username);
            headers.put(TokenUtils.X_AUTH_PASSWORD, password);

            // Creamos el body
            JSONObject body = this.getRegistryRequestBody(name, surename, username);

            progressDialog = ProgressDialog.show(this, "Registrando", "Por favor espere...");
            HttpUtils.post(this, "/api/v1/user/registry", new StringEntity(body.toString()), headers, new AppJsonHttpResponseHandler(this){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.d("Response", response.toString());
                    progressDialog.hide();
                    showRegistrySuccessMessage();
                }
                @Override
                public void onError(int statusCode, Header[] headers, Throwable throwable, JSONObject error) {
                    progressDialog.hide();
                    try {
                        String errorMessage = error != null ? error.getString("message") : "";
                        Log.d("Error", errorMessage);
                        AlertUtils.showOkAlertMessage(this.getContext(), REGISTRY_ERROR_TITLE, errorMessage);
                    }
                    catch (Exception e){
                        AlertUtils.showOkAlertMessage(this.getContext(), REGISTRY_ERROR_TITLE, throwable.getMessage());
                    }
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void showRegistrySuccessMessage() {
        AlertUtils.showOkAlertMessage(this, "Gracias por registrarse",
            "Su registro fu exitoso, ingrese a la aplicacion con el mail y contraseña provistos",
            new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    goToLocalAccountLoginPage();
                }});
    }

    private JSONObject getRegistryRequestBody(String name, String surename, String email) {
        JSONObject body = new JSONObject();
        try {
            body.put("name", name);
            body.put("surename", surename);
            body.put("email", email);
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return body;
    }

    private void goToLocalAccountLoginPage() {
        Intent intent = new Intent(this, LocalAccountLoginActivity.class);
        startActivity(intent);
    }
}
