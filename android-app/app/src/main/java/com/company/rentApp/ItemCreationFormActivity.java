package com.company.rentApp;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import REST.AppJsonHttpResponseHandler;
import REST.HttpUtils;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import utils.AlertUtils;

public class ItemCreationFormActivity extends AppCompatActivity {

    private HashMap<Integer,String> spinnerMap = new HashMap<Integer, String>();
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_creation_form);

        this.fetchCategories();
    }

    /**
     * Manda a guardar un item
     */
    public void saveItem(View view) {

        // Category
        Spinner spinner=(Spinner) findViewById(R.id.categorySpinner);
        String categoryId = this.spinnerMap.get(spinner.getSelectedItemPosition());

        // Title
        EditText mEdit   = (EditText)findViewById(R.id.titleInput);
        String title = mEdit.getText().toString();

        // Description
        mEdit   = (EditText)findViewById(R.id.descriptionInput);
        String description = mEdit.getText().toString();

        try {
            JSONObject jsonParams = this.buildItemJsonObject(categoryId, title, description);
            StringEntity entity = new StringEntity(jsonParams.toString());
            Log.d("ITEM_CREATION", "Creando el item: " + jsonParams.toString());

            progressDialog = ProgressDialog.show(this, "Creando el item", "Por favor espere...");
            HttpUtils.post(this.getApplicationContext(),"/api/v1/item", entity, new AppJsonHttpResponseHandler(this){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    try {
                        progressDialog.hide();
                        Log.d("Response",response.toString());
                        showAlertMessage("Item creado", "Su item fue creado con id: " + response.get("id"));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onError(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    progressDialog.hide();
                    Log.d("Error: ",errorResponse.toString());
                    showAlertMessage("Error en la creación del item", "Por favor, intente nuevamente");
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Trae las categorias del servidor
     */
    private void fetchCategories() {
        progressDialog = ProgressDialog.show(this, "Cargando", "Por favor espere...");
        HttpUtils.get(this.getApplicationContext(), "/api/v1/categories", null, new AppJsonHttpResponseHandler(this){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    progressDialog.hide();
                    fillCategoriesDropDown(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void onError(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse){
                progressDialog.hide();
                Log.d("Error", errorResponse.toString());
            }

        });
    }

    /**
     * Completa el dropdown de categorias con la data de categorias
     */
    private void fillCategoriesDropDown(JSONObject json) throws JSONException {
        JSONArray jArray = json.getJSONArray("categories");
        String[] spinnerArray = new String[jArray.length()];
        for (int i = 0; i < jArray.length(); i++)
        {
            this.spinnerMap.put(i,jArray.getJSONObject(i).get("id").toString());
            spinnerArray[i] = jArray.getJSONObject(i).get("description").toString();
        }
        ArrayAdapter<String> adapter =new ArrayAdapter<String>(this.getApplicationContext(),android.R.layout.simple_spinner_dropdown_item, spinnerArray);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Spinner dropdown = (Spinner)findViewById(R.id.categorySpinner);
        dropdown.setAdapter(adapter);
    }

    private JSONObject buildItemJsonObject(String categoryId, String title, String description) throws JSONException {
        JSONObject jsonParams = new JSONObject();
        jsonParams.put("category", this.buildJsonIdObject(categoryId));
        jsonParams.put("title", title);
        jsonParams.put("description", description);
        jsonParams.put("geoLat", -34.5098168);  // TODO: Hardcode por ahora
        jsonParams.put("geoLon", -58.4937188);
        return jsonParams;
    }

    private JSONObject buildJsonIdObject(Object id) throws JSONException {
        JSONObject jsonParams = new JSONObject();
        jsonParams.put("id", id);
        return jsonParams;
    }

    /**
     * Muestra un simple alert con boton de ok
     */
    private void showAlertMessage(String title, String message){
        AlertUtils.showOkAlertMessage(this, title, message);
    }
}
