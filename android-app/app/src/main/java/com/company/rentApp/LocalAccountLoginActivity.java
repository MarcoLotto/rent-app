package com.company.rentApp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import REST.AppJsonHttpResponseHandler;
import REST.HttpUtils;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import utils.AlertUtils;
import utils.TokenUtils;

public class LocalAccountLoginActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_local_account_login);
    }

    // Es llamado desde el layout
    public void onLoginButtonSelected(View view){
        EditText username = (EditText) findViewById(R.id.usernameInput);
        EditText password = (EditText) findViewById(R.id.passwordInput);
        String usernameValue = username.getText().toString();
        String passwordValue = password.getText().toString();

        if(!usernameValue.isEmpty() && !passwordValue.isEmpty()){
            this.makeLoginRequest(usernameValue, passwordValue);
        }
    }

    public void onGoToRegistrationButtonSelected(View view){
        Intent intent = new Intent(this, LocalAccountRegistryActivity.class);
        startActivity(intent);
    }

    private void makeLoginRequest(String username, String password) {
        try {
            Map<String, String> headers = new HashMap<>();
            headers.put(TokenUtils.X_AUTH_USERNAME, username);
            headers.put(TokenUtils.X_AUTH_PASSWORD, password);

            progressDialog = ProgressDialog.show(this, "Logueando al servidor", "Por favor espere...");
            HttpUtils.post(this, "/api/v1/login", new StringEntity(""), headers, new AppJsonHttpResponseHandler(this){
                @Override
                public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                    Log.d("Response", response.toString());
                    for(Header header : headers){
                        if(header.getName().equals(TokenUtils.X_AUTH_TOKEN_PARAM)){
                            saveLoginToken(header.getValue());
                        }
                    }
                    progressDialog.hide();
                    evaluateAlreadyExistantLogin();
                }
                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject error) {
                    progressDialog.hide();
                    // TODO: Podría ser otro error que no sea problema de la contraseña
                    AlertUtils.showOkAlertMessage(this.getContext(), "Login fallido", "Por favor revise su usuario y contraseña");
                }
            });
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void saveLoginToken(String token){
        TokenUtils.saveToken(this.getApplicationContext(), token);
    }

    private void evaluateAlreadyExistantLogin() {
        // Si tenemos token, vamos a la pantalla principal
        if(TokenUtils.getToken(this.getApplicationContext()) != null) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }
}
