package fragments;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.company.rentApp.R;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import REST.AppJsonHttpResponseHandler;
import REST.HttpUtils;
import adapters.StableArrayAdapter;
import cz.msebera.android.httpclient.Header;

public class RentSearchFragment extends Fragment implements View.OnClickListener {

    public static final String TAB_DISPLAY_NAME = "Buscar Alquileres";
    private ProgressDialog progressDialog;
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        this.view = inflater.inflate(R.layout.rent_search_view, container, false);

        this.declareButtonsListeners(this.view);
        return this.view;
    }

    private void buildListAdapter(View view,JSONArray elements) throws JSONException {
        final ListView listview = (ListView) view.findViewById(R.id.listview);

        final ArrayList<String> list = new ArrayList<String>();
        for (int i=0; i < elements.length(); i++) {
            list.add(elements.getJSONObject(i).getString("title"));
        }
        final StableArrayAdapter adapter = new StableArrayAdapter(this.getContext(),
                android.R.layout.simple_list_item_1, list);
        listview.setAdapter(adapter);
    }

    @Override
    public void onCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
    }

    private void searchForItems(){
        final View auxView = this.view;
        progressDialog = ProgressDialog.show(this.getContext(), "Cargando", "Por favor espere...");
        HttpUtils.get(this.getContext(), "/api/v1/item/search/title", this.buildSearchRequestParams(), new AppJsonHttpResponseHandler(this.getContext()){
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                Log.d("Response", response.toString());
                try {
                    buildListAdapter(auxView, response.getJSONArray("content"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.hide();
            }
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject error) {
                if(error != null) {
                    Log.e("Error", error.toString());
                }
                progressDialog.hide();
            }
        });
    }

    private RequestParams buildSearchRequestParams() {
        RequestParams params = new RequestParams();
        String title = this.getInputText(R.id.searchInput);
        Log.d("SEARCH_ITEM_TITLE", title);
        params.add("title", title);
        return params;
    }

    private String getInputText(int idOnView){
        EditText mEdit = (EditText)this.view.findViewById(idOnView);
        return mEdit.getText().toString();
    }

    private void declareButtonsListeners(View view) {
        Button b = (Button) view.findViewById(R.id.searchButton);
        b.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.searchButton:
                this.searchForItems();
                break;
        }
    }
}
