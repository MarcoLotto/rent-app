package fragments;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.company.rentApp.ItemCreationFormActivity;
import com.company.rentApp.R;


public class MyRentsFragment extends Fragment implements View.OnClickListener {

    public static final String TAB_DISPLAY_NAME = "Mis Alquileres";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.my_rents_view, container, false);
        this.declareButtonsListeners(view);
        return view;
    }

    private void declareButtonsListeners(View view) {
        Button b = (Button) view.findViewById(R.id.goToItemCreationFormButton);
        b.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.goToItemCreationFormButton:
                goToItemCreationForm();
                break;
        }
    }

    /**
     * Abre el activity del form de creacion de item
     */
    private void goToItemCreationForm(){
        Intent intent = new Intent(this.getContext(), ItemCreationFormActivity.class);
        startActivity(intent);
    }
}
