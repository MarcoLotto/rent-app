# Compilamos la app
chmod +x ../gradlew
../gradlew build
sudo mv ../build/libs/gs-spring-boot-0.1.0.jar ./gs-spring-boot.jar

# Creamos la imagen de docker
docker build . -t marcolotto/rent-app

# Subimos la imagen a dockerhub
docker login
docker push marcolotto/rent-app



