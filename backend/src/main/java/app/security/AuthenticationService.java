package app.security;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.UUID;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import app.domain.User;
import app.domain.UserAccessToken;
import app.domain.UserAuthenticable;
import app.domain.ItemElastic;
import app.domain.ExternalUserAuthentication;
import app.domain.LocalUserAuthentication;
import app.exceptions.BusinessException;
import app.repositories.jpa.UserAccessTokenRepository;
import app.repositories.elasticsearch.ItemElasticRepository;
import app.repositories.jpa.ExternalUserAuthenticationRepository;
import app.repositories.jpa.LocalUserAuthenticationRepository;
import app.repositories.jpa.UserRepository;

@Service
public class AuthenticationService {
	
	// Define cuanto dura un token antes de expirar
	private static final int TOKEN_EXPIRATION_DAYS = 30;
	
	private LocalUserAuthenticationRepository localUserAuthenticationRepository;
	private ExternalUserAuthenticationRepository externalUserAuthenticationRepository;
	private UserAccessTokenRepository userAccessTokenRepository;
	private UserRepository userRepository;
	private PasswordEncoder passwordEncoder;
	private ItemElasticRepository userElasticRepository;
	private TokenCacheService tokenCacheService;
	
	@Autowired
	public AuthenticationService(LocalUserAuthenticationRepository localUserAuthenticationRepository,
			ExternalUserAuthenticationRepository externalUserAuthenticationRepository, UserRepository userRepository, 
			PasswordEncoder passwordEncoder, UserAccessTokenRepository userAccessTokenRepository,
			ItemElasticRepository userElasticRepository, TokenCacheService tokenCacheService){
		this.localUserAuthenticationRepository = localUserAuthenticationRepository;
		this.externalUserAuthenticationRepository = externalUserAuthenticationRepository;
		this.userAccessTokenRepository = userAccessTokenRepository;
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.userElasticRepository = userElasticRepository;
		this.tokenCacheService = tokenCacheService;
	}
	
	/**
	 * Autenticamos un usuario para una cuenta local, mediante su usuario y contraseña
	 */
	@Transactional
	public AuthenticationWithToken authenticate(String username, String password) {
		// Buscamos al usuario por username
		LocalUserAuthentication userAuthentication = this.localUserAuthenticationRepository.findOneByUsername(username);
		if(userAuthentication == null){
			return null;
		}
		// Validamos que las contraseñas coincidan
		this.validatePasswordMatch(userAuthentication, password);
		
		// Regeneramos el access token y generamos la authenticacion
		this.generateAndSaveAccessToken(userAuthentication);
		return this.buildAuthenticationWithToken(userAuthentication.getAccessToken(), userAuthentication.getUsername());
	}
	
	/**
	 * Verifica si existe el usuario con credenciales externas (facebook, etc) en el sistema local. De ser asi devuelve una autenticacion 
	 */
	@Transactional
	public AuthenticationWithToken retrieveExternalAuthentication(Long externalId, String client, String externalUsername) {
		ExternalUserAuthentication externalAuth = this.externalUserAuthenticationRepository.findOneByExternalIdAndClient(externalId, client);
		if(externalAuth != null){
			// Regeneramos el access token y generamos la authenticacion
			this.generateAndSaveAccessToken(externalAuth);
			return this.buildAuthenticationWithToken(externalAuth.getAccessToken(), externalUsername);
		}
		return null;
	}
	
	/**
	 * Registra un usuario con cuenta local en el sistema
	 */
	public LocalUserAuthentication registerLocalUser(String username, String password, User user) {
		
		// Validamos que no exista un usuario con el mismo username
		if(this.localUserAuthenticationRepository.findOneByUsername(username) != null){
			throw new BusinessException("User is already registered");
		}
		// Creamos el usuario y autenticacion local
		return this.createUserAndLocalAuthentication(username, password, user);
	}
	
	/**
	 * Registra un usuario externo en el sistema (ej. facebook, google, etc)
	 */
	public AuthenticationWithToken registerExternalUser(User user, Long externalId, String client) {
		return this.createUserAndExternalAuthentication(user, externalId, client);
	}
	
	/**
	 * Desloguea un usuario
	 */
	public void userLogout(User user){
		// Cambiamos la fecha del token a la actual para invalidarlo
		UserAccessToken token = this.userAccessTokenRepository.findOneByUserId(user.getId());
		token.setTokenExpiration(new Date());
		this.userAccessTokenRepository.save(token);
		
		// Invalidamos el token en la cache
		this.tokenCacheService.invalidate(token.getTokenValue());
	}
	
	/**
	 * Devuelve una autorización del usuario en base al token o null si no existe el token
	 */
	@Transactional
	public AuthenticationWithToken getUserAuthentication(String token){
		UserAccessToken userAccessToken = this.getTokenInfo(token);
		if(userAccessToken != null){
			UserAccessToken localAuth = this.userAccessTokenRepository.findOneByUserId(userAccessToken.getUserId());
			return this.buildAuthenticationWithToken(localAuth, localAuth.getUserId().toString());
		}
		return null;
	}
	
	@Transactional
	private LocalUserAuthentication createUserAndLocalAuthentication(String username, String password, User user) {
		// Guardamos la info de usuario
		User savedUser = this.userRepository.save(user);
		
		// Guardamos los datos de autenticacion del usuario
		LocalUserAuthentication userAuthentication = this.buildUserAuthentication(username, password, savedUser);
		this.localUserAuthenticationRepository.save(userAuthentication);
		return userAuthentication;
	}
	
	@Transactional
	private AuthenticationWithToken createUserAndExternalAuthentication(User user, Long externalId, String client) {
		
		// Guardamos la info de usuario
		User savedUser = this.userRepository.save(user);
				
		// Generamos la relacion entre el user local y los credenciales externos, generamos el token
		ExternalUserAuthentication externalAuth = new ExternalUserAuthentication();
		externalAuth.setUserId(user.getId());
		externalAuth.setClient(client);
		externalAuth.setExternalId(externalId);
		
		this.generateAndSaveAccessToken(externalAuth);
		this.externalUserAuthenticationRepository.save(externalAuth);
		return this.buildAuthenticationWithToken(externalAuth.getAccessToken(), this.buildUsernameFromUser(user));
	}
	
	private String buildUsernameFromUser(User user){
		return user.getName() + " " + user.getSurename();
	}
	
	private AuthenticationWithToken buildAuthenticationWithToken(UserAccessToken userAccessToken, String referenceUsername){
		return new AuthenticationWithToken(userAccessToken.getUserId(), referenceUsername,
				userAccessToken.getTokenValue(), userAccessToken.getTokenExpiration());
	}

	private void validatePasswordMatch(LocalUserAuthentication userAuthentication, String password) {
		if(!this.passwordEncoder.matches(password, userAuthentication.getHashedPassword())){
			throw new BusinessException("Username or password is invalid");
		}
	}
	
	private LocalUserAuthentication buildUserAuthentication(String username, String password, User user){
		LocalUserAuthentication userAuthentication = new LocalUserAuthentication(username, user);
		userAuthentication.setUser(user);
		userAuthentication.setHashedPassword(this.passwordEncoder.encode(password));
		this.generateAndSaveAccessToken(userAuthentication);
		return userAuthentication;
	}
	
	/**
	 * Genera un nuevo access token para el usuario y lo guarda en la DB
	 */
	private UserAccessToken generateAndSaveAccessToken(UserAuthenticable userAuthentication) {
		UserAccessToken userAccessToken = new UserAccessToken(userAuthentication.getUserId());
		userAccessToken.setTokenValue(this.generateNewToken());
		userAccessToken.setTokenExpiration(this.getTokenExpirationDate());
		userAccessToken = this.userAccessTokenRepository.save(userAccessToken);
		userAuthentication.setAccessToken(userAccessToken);
		return userAccessToken;
	}
	
	private String generateNewToken() {
        return UUID.randomUUID().toString();
    }

	private Date getTokenExpirationDate() {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(new Date());
		cal.add(Calendar.DATE, TOKEN_EXPIRATION_DAYS);
		return cal.getTime();
	}

	private UserAccessToken getTokenInfo(String tokenToFind){
		return this.userAccessTokenRepository.findOneByTokenValue(tokenToFind);
	}
	
}
