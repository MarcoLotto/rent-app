package app.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public class AuthenticationWithToken implements Authentication {

	private static final long serialVersionUID = 1L;
	
	private Long userId;
	private String username;
	private String token;
	private boolean authenticated = true;
	private Date tokenExpirationDate;
	private List<? extends GrantedAuthority> authorities = new ArrayList<>();
	
	public AuthenticationWithToken(){
		
	}
	
	public AuthenticationWithToken(Long userId, String username, String token, Date tokenExpiration){
		this.userId = userId;
		this.username = username;
		this.token = token;
		this.tokenExpirationDate = tokenExpiration;
	}

	@Override
	public String getName() {
		return this.username;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}

	@Override
	public Object getCredentials() {
		return this.token;
	}

	@Override
	public Object getDetails() {
		return this.username;
	}

	@Override
	public Object getPrincipal() {
		return this.userId;
	}

	@Override
	public boolean isAuthenticated() {
		return this.token != null && this.tokenExpirationDate != null && !this.token.isEmpty() &&
				this.authenticated && new Date().before(this.tokenExpirationDate);
	}
	
	public void setToken(String token){
		this.token = token;
	}
	
	public void setName(String name) {
		this.username = name;
	}
	
	public void setAuthorities(List<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	public Date getTokenExpirationDate() {
		return tokenExpirationDate;
	}

	public void setTokenExpirationDate(Date tokenExpirationDate) {
		this.tokenExpirationDate = tokenExpirationDate;
	}

	public String getToken() {
		return token;
	}

	@Override
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
		this.authenticated = isAuthenticated;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
}
