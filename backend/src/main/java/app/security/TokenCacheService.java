package app.security;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class TokenCacheService {

	// TODO: Esto esta en memoria, cambiar a una cache externa
    private Map<String, Authentication> restApiAuthTokenCache = new HashMap<>();


    public void store(String token, Authentication authentication) {
        restApiAuthTokenCache.put(token, authentication);
    }

    public Authentication retrieve(String token) {
    	Authentication auth = this.restApiAuthTokenCache.get(token);
    	
    	// Si la autorizacion ya no es valida, la borramos de la cache
    	if(auth != null && !auth.isAuthenticated()){
    		this.invalidate(token);
    		return null;
    	}
    	return auth;
    }
    
    public void invalidate(String token){
    	this.restApiAuthTokenCache.remove(token);
    }
}
