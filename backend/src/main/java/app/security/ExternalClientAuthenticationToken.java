package app.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

public class ExternalClientAuthenticationToken extends UsernamePasswordAuthenticationToken {

	private static final long serialVersionUID = 2591725309259669119L;
	
	public ExternalClientAuthenticationToken(Object principal, Object credentials) {
		super(principal, credentials);
	}
}
