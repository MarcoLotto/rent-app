package app.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

@Component
public class DomainUsernamePasswordAuthenticationProvider implements AuthenticationProvider {

    private TokenCacheService tokenCacheService;
    private AuthenticationService authenticationService;

    @Autowired	
    public DomainUsernamePasswordAuthenticationProvider(TokenCacheService tokenCacheService, AuthenticationService authenticatorService) {
        this.tokenCacheService = tokenCacheService;
        this.authenticationService = authenticatorService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Optional<String> username = (Optional) authentication.getPrincipal();
        Optional<String> password = (Optional) authentication.getCredentials();

        if (!username.isPresent() || !password.isPresent()) {
            throw new BadCredentialsException("Invalid Domain User Credentials");
        }

        AuthenticationWithToken resultOfAuthentication = this.authenticationService.authenticate(username.get(), password.get());
        if(resultOfAuthentication != null && resultOfAuthentication.isAuthenticated()){
        	this.tokenCacheService.store((String) resultOfAuthentication.getCredentials(), resultOfAuthentication);
        }
        return resultOfAuthentication;
    }

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
