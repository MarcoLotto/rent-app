package app.security;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsUtils;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	private TokenAuthenticationProvider tokenAuthenticationProvider;
	private DomainUsernamePasswordAuthenticationProvider domainUsernamePasswordAuthenticationProvider;
	private ExternalClientAuthenticationProvider externalClientAuthenticationProvider;
	
	@Autowired
	public SecurityConfiguration(TokenAuthenticationProvider tokenAuthenticationProvider, 
			DomainUsernamePasswordAuthenticationProvider domainUsernamePasswordAuthenticationProvider,
			ExternalClientAuthenticationProvider externalClientAuthenticationProvider){
		this.tokenAuthenticationProvider = tokenAuthenticationProvider;
		this.domainUsernamePasswordAuthenticationProvider = domainUsernamePasswordAuthenticationProvider;
		this.externalClientAuthenticationProvider = externalClientAuthenticationProvider;
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
	    http.
	      csrf().disable()
          .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
          .and().cors().and()
          .antMatcher("/**").authorizeRequests()
	      .requestMatchers(CorsUtils::isPreFlightRequest).permitAll()
	      .anyRequest().authenticated()
	      .and()
	      .anonymous().disable()
	      .exceptionHandling().authenticationEntryPoint(unauthorizedEntryPoint());

	    http.addFilterBefore(new AuthenticationFilter(this.authenticationManager()), BasicAuthenticationFilter.class);
	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
	    web.ignoring().antMatchers("/api/v1/user/registry");
	}
	
	@Bean
	public AuthenticationEntryPoint unauthorizedEntryPoint() {
	    return (request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
	    auth.authenticationProvider(this.tokenAuthenticationProvider).
	         authenticationProvider(this.domainUsernamePasswordAuthenticationProvider).
	         authenticationProvider(this.externalClientAuthenticationProvider);
	}
}
