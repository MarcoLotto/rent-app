package app.security;

import java.io.IOException;
import java.util.Optional;

import javax.security.sasl.AuthenticationException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.netty.handler.codec.http.HttpMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.filter.GenericFilterBean;
import org.springframework.web.util.UrlPathHelper;

import com.fasterxml.jackson.databind.ObjectMapper;

public class AuthenticationFilter extends GenericFilterBean {
	
	public static final String X_AUTH_TOKEN_PARAM = "X-Auth-Token";
	public static final String X_AUTH_USERNAME_PARAM = "X-Auth-Username";
	public static final String X_AUTH_PASSWORD_PARAM = "X-Auth-Password";
	private static final String X_AUTH_EXTERNAL_CLIENT_PARAM = "X-Auth-External-Client";
	private static final String X_AUTH_EXTERNAL_TOKEN_PARAM = "X-Auth-External-Token";
	
	private static final String LOGIN_URL_PATH = "/api/v1/login";
	
	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	private AuthenticationManager authenticationManager;

	public AuthenticationFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
	    HttpServletRequest httpRequest = (HttpServletRequest) request;
	    HttpServletResponse httpResponse = (HttpServletResponse) response;

	    Optional<String> username = Optional.ofNullable(httpRequest.getHeader(X_AUTH_USERNAME_PARAM));
	    Optional<String> password = Optional.ofNullable(httpRequest.getHeader(X_AUTH_PASSWORD_PARAM));
	    Optional<String> externalClient = Optional.ofNullable(httpRequest.getHeader(X_AUTH_EXTERNAL_CLIENT_PARAM));
	    Optional<String> externalToken = Optional.ofNullable(httpRequest.getHeader(X_AUTH_EXTERNAL_TOKEN_PARAM));
	    Optional<String> token = Optional.ofNullable(httpRequest.getHeader(X_AUTH_TOKEN_PARAM));

	    String resourcePath = new UrlPathHelper().getPathWithinApplication(httpRequest);

	    try {
	    	// Si tenemos una llamada a login (por cuenta local o externa), lo procesamos
	        if (this.isPostToAuthenticate(httpRequest, resourcePath)) {
	        	if(username.isPresent() && password.isPresent()){
	        		logger.debug("Trying to authenticate user " + username.get() + " by X-Auth-Username method");
	        		this.processUsernamePasswordAuthentication(httpResponse, username, password);
	        	}
	        	else if(externalClient.isPresent() && externalToken.isPresent()){
	        		logger.debug("Trying to authenticate user by X-Auth-External-Client method - " + externalClient.get());
	        		this.processExternalClientAuthentication(httpResponse, externalClient, externalToken);
	        	}
	            return;
	        }
	        // Si no es llamada a login, lo hacemos pasar por el token filer
	        if (token.isPresent()) {
	            logger.debug("Trying to authenticate user by X-Auth-Token method. Token: {}", token);
	            processTokenAuthentication(token);
	        }
	        logger.debug("AuthenticationFilter is passing request down the filter chain");
	        chain.doFilter(request, response);
	    } 
	    catch(InternalAuthenticationServiceException e) {
	        logger.error("Internal authentication service exception", e);
	        this.setResponseErrorAndClearContext(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e, httpResponse);
	    } 
	    catch(AuthenticationException e) {
	        this.setResponseErrorAndClearContext(HttpServletResponse.SC_UNAUTHORIZED, e, httpResponse);
	    }
	    catch(BadCredentialsException e){
	    	this.setResponseErrorAndClearContext(HttpServletResponse.SC_UNAUTHORIZED, e, httpResponse);
	    }
	}

	private void setResponseErrorAndClearContext(int httpErrorCode, Exception e, HttpServletResponse httpResponse) throws IOException{
		SecurityContextHolder.clearContext();
        httpResponse.sendError(httpErrorCode, e.getMessage());
	}

	private void processUsernamePasswordAuthentication(HttpServletResponse httpResponse, Optional username, Optional password) throws IOException {
	    Authentication resultOfAuthentication = this.tryToAuthenticateWithUsernameAndPassword(username, password);
	    SecurityContextHolder.getContext().setAuthentication(resultOfAuthentication);
	    this.writeTokenToResponse(httpResponse, resultOfAuthentication);
	}
	
	private void processExternalClientAuthentication(HttpServletResponse httpResponse, Optional<String> externalClient,
			Optional<String> externalToken) throws IOException {
		 Authentication resultOfAuthentication = this.tryToAuthenticateWithExternalClient(externalClient, externalToken);
		 SecurityContextHolder.getContext().setAuthentication(resultOfAuthentication);
		 this.writeTokenToResponse(httpResponse, resultOfAuthentication);
	}

	private void writeTokenToResponse(HttpServletResponse httpResponse, Authentication authentication) throws IOException {
		// Escribimos el token generado en la respuesta del pedido
	    httpResponse.setStatus(HttpServletResponse.SC_OK);
	    httpResponse.addHeader("Content-Type", "application/json");
	    httpResponse.addHeader(X_AUTH_TOKEN_PARAM, (String) authentication.getCredentials());
	    httpResponse.getWriter().write("{}");
	    httpResponse.getWriter().flush();
	    httpResponse.getWriter().close();
	}

	private Authentication tryToAuthenticateWithUsernameAndPassword(Optional username, Optional password) {
	    UsernamePasswordAuthenticationToken requestAuthentication = new UsernamePasswordAuthenticationToken(username, password);
	    return tryToAuthenticate(requestAuthentication);
	}
	
	private Authentication tryToAuthenticateWithExternalClient(Optional<String> externalClient, Optional<String> externalToken) {
		ExternalClientAuthenticationToken requestAuthentication = new ExternalClientAuthenticationToken(externalClient, externalToken);
	    return tryToAuthenticate(requestAuthentication);
	}

	
	private void processTokenAuthentication(Optional<String> token) {
		PreAuthenticatedAuthenticationToken requestAuthentication = new PreAuthenticatedAuthenticationToken(token, token);
		Authentication authentication = tryToAuthenticate(requestAuthentication);
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}
	
	private Authentication tryToAuthenticate(Authentication requestAuthentication){
		return this.authenticationManager.authenticate(requestAuthentication);
	}
	
	private boolean isPostToAuthenticate(HttpServletRequest httpRequest, String resourcePath) {
		return httpRequest.getMethod().equals(HttpMethod.POST.toString()) && resourcePath.contains(LOGIN_URL_PATH);
	}
}

