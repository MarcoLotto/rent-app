package app.security;

import java.util.Optional;
import app.security.TokenCacheService;

import org.apache.tomcat.util.http.parser.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;

@Component
public class TokenAuthenticationProvider implements AuthenticationProvider {

    private TokenCacheService tokenCacheService;
    private AuthenticationService authenticationService;

    @Autowired
    public TokenAuthenticationProvider(TokenCacheService tokenCacheService, 
    		AuthenticationService authenticationService) {
        this.tokenCacheService = tokenCacheService;
        this.authenticationService = authenticationService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) {
        Optional<String> token = (Optional) authentication.getPrincipal();
        if (!token.isPresent() || token.get().isEmpty()) {
            throw new BadCredentialsException("Invalid token");
        }
        Authentication retrievedAuthentication = this.retriveAuthenticationFromToken(token);
        if (retrievedAuthentication == null) {
            throw new BadCredentialsException("Invalid token or token expired");
        }
        return retrievedAuthentication;
    }
    
	private Authentication retriveAuthenticationFromToken(Optional<String> token) {
		Authentication authentication = this.tokenCacheService.retrieve(token.get());
		if(authentication == null){
			authentication = this.authenticationService.getUserAuthentication(token.get());
		}
		return authentication;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(PreAuthenticatedAuthenticationToken.class);
	}
}
