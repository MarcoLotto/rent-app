package app.security;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import app.domain.User;

/**
 *	Provider para validar autenticacion de clientes externos (ej. Facebook, Google, etc)
 */
@Component
public class ExternalClientAuthenticationProvider implements AuthenticationProvider {

    private static final String FACEBOOK_CLIENT = "facebook";
    private static final String FACEBOOK_PROFILE_ENDPOINT = "https://graph.facebook.com/me";
    
    private Logger log = LoggerFactory.getLogger(this.getClass());
    
	private TokenCacheService tokenCacheService;
    private AuthenticationService authenticationService;

    @Autowired	
    public ExternalClientAuthenticationProvider(TokenCacheService tokenCacheService, AuthenticationService authenticatorService) {
        this.tokenCacheService = tokenCacheService;
        this.authenticationService = authenticatorService;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        Optional<String> client = (Optional) authentication.getPrincipal();
        Optional<String> token = (Optional) authentication.getCredentials();

        if (!client.isPresent() || !token.isPresent()) {
            throw new BadCredentialsException("Invalid ExternalClient User Credentials");
        }

        AuthenticationWithToken resultOfAuthentication = this.authenticateWithExternalClient(client.get(), token.get());
        if(resultOfAuthentication != null && resultOfAuthentication.isAuthenticated()){
        	this.tokenCacheService.store((String) resultOfAuthentication.getCredentials(), resultOfAuthentication);
        }
        return resultOfAuthentication;
    }

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(ExternalClientAuthenticationToken.class);
	}
	
	private AuthenticationWithToken authenticateWithExternalClient(String client, String token) {
		if(client.equals(FACEBOOK_CLIENT)){
			FacebookProfileContainer response = this.makeFacebookProfileRequest(token);
			if(response != null && response.getId() != null){
				return this.buildLocalAuthenticationWithFacebookCredentials(response, token);
			}
		}
		// Acá podrían ir mas clientes (google, etc)
		
		return null;
	}
	
	/**
	 * Buscamos los credenciales externos en el sistema. Si no existe el usuario lo creamos. Devuelve una autorizacion
	 */
	private AuthenticationWithToken buildLocalAuthenticationWithFacebookCredentials(FacebookProfileContainer response, String token) {
		AuthenticationWithToken auth = this.authenticationService.retrieveExternalAuthentication(response.getId(), FACEBOOK_CLIENT, response.getName());
		if(auth != null){
			log.info("User " + response.getName() + " (" + response.getId() + ") has logged in via facebook");
			return auth;
		}
		// No existe el usuario en el sistema, lo creamos
		log.info("User " + response.getName() + " (" + response.getId() + ") is been registered via facebook");
		return this.authenticationService.registerExternalUser(this.buildNewUser(response, token), response.getId(), FACEBOOK_CLIENT);
	}

	/**
	 * Construye el usuario a partir de los datos del cliente externo
	 */
	private User buildNewUser(FacebookProfileContainer response, String token) {
		User user = new User();
		user.setName(response.getName());
		user.setEmail(response.getName());
		user.setSurename(response.getName());
		// TODO Hacer llamada a facebook para conseguir los demas datos
		return user;
	}

	private FacebookProfileContainer makeFacebookProfileRequest(String facebookToken){
		RestTemplate restTemplate = new RestTemplate();
		String endpoint = FACEBOOK_PROFILE_ENDPOINT + "?" + this.buildFacebookCredentials(facebookToken);
		return restTemplate.getForObject(endpoint, FacebookProfileContainer.class);
	}
	
	private String buildFacebookCredentials(String facebookToken) {
		return "access_token=" + facebookToken;
	}

	private Object makeExternalGetRequest(String endpoint, Class<?> expectedContainer){
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(endpoint, expectedContainer);
	}
}
