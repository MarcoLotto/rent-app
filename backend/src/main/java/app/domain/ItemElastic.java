package app.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.geo.Point;

@Document(indexName = "items", type = "item")
public class ItemElastic {

    @Id
    private Long id;
    private String title;
    private Long categoryId;
    private Point location;

    public ItemElastic() {
    }

    public ItemElastic(Item item) {
        this.setId(item.getId());
        this.setTitle(item.getTitle());
        this.setCategoryId(item.getCategory().getId());
        this.setLocation(new Point(item.getGeoLat(), item.getGeoLon()));
    }

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Point getLocation() {
		return location;
	}

	public void setLocation(Point location) {
		this.location = location;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public ItemDTO convertToDTO() {
		ItemDTO dto = new ItemDTO();
		dto.setCategoryId(this.getCategoryId());
		dto.setTitle(this.getTitle());
		dto.setId(this.getId());
		dto.setLocation(this.getLocation());
		return dto;
	}
}
