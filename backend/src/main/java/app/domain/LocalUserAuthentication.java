package app.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Provee autenticacion contra usuarios con cuenta local (usuario y pass)
 */
@Entity
@Table(name = "local_user_authentication")
public class LocalUserAuthentication implements Serializable, UserAuthenticable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name="user_id", nullable=false)
	private Long userId;
	
	@Column(name = "username", nullable=false)
	private String username;
	
	@Column(name = "hashed_password", nullable=false)
	private String hashedPassword;
	
	@ManyToOne
	@JoinColumn(name = "access_token_id", nullable=false)
	private UserAccessToken accessToken;
	
	@Column(name = "date_created", nullable=false)
	private Date dateCreated;
	
	
	public LocalUserAuthentication(){
		this.dateCreated = new Date();
	}
	
	public LocalUserAuthentication(String username, User user){
		this.dateCreated = new Date();
		this.username = username;
		this.userId = user.getId();
	}

	public Long getUserId() {
		return this.userId;
	}
	
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setUser(User user) {
		this.userId = user.getId();
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getHashedPassword() {
		return hashedPassword;
	}

	public void setHashedPassword(String hashedPassword) {
		this.hashedPassword = hashedPassword;
	}

	public UserAccessToken getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(UserAccessToken accessToken) {
		this.accessToken = accessToken;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
}
