package app.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Define la relacion entre usuarios locales y credenciales externos (ej. facebook, google)
 */
@Entity
@Table(name = "external_user_authentication")
public class ExternalUserAuthentication implements Serializable, UserAuthenticable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @Column(name="user_id", nullable=false)
	private Long userId;
	
	@Column(name = "client", nullable=false)
	private String client;
	
	@Column(name = "external_id", nullable=false)
	private Long externalId;
	
	@ManyToOne
	@JoinColumn(name = "access_token_id", nullable=false)
	private UserAccessToken accessToken;
	

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public Long getExternalId() {
		return externalId;
	}

	public void setExternalId(Long externalId) {
		this.externalId = externalId;
	}

	public UserAccessToken getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(UserAccessToken accessToken) {
		this.accessToken = accessToken;
	}
}
