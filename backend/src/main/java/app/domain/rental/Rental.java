package app.domain.rental;

import javax.persistence.*;
import java.util.Date;
import app.domain.User;
//import app.domain.Item;

@Entity
@Table(name = "rental")
public class Rental {

    /*
    @ManyToOne
    @JoinColumn(name="owner_id", nullable=false)
    private User owner;
    */

    @ManyToOne
    @JoinColumn(name="borrower_id", nullable=false)
    private User borrower;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name="rental_created", nullable=false)
    private Date rentalDate;

    @Column(name="return_date", nullable=false)
    private Date returnDate;

    @Column(name="itemId", nullable = false)
    private Long itemId;

    public Rental(Date returnDate,User owner, User borrower) {
        this.rentalDate = new Date();
        this.returnDate = returnDate;
        //this.owner = owner;
        this.borrower = borrower;
    }
/*
    public void setOwner(User owner) { this.owner = owner; }

    public User getOwner() { return this.owner; }
*/
    public void setBorrower(User borrower) { this.borrower = borrower; }

    public User getBorrower() { return this.borrower; }

    public void setRentalDate(Date rentalDate) { this.rentalDate = rentalDate; }

    public Date getRentalDate() { return this.rentalDate; }

    public void setReturnDate(Date returnDate) { this.returnDate = returnDate; }

    public Date getReturnDate() { return this.returnDate; }

    public void setItemId(Long item) { this.itemId = itemId; }

    public Long getItemId() { return this.itemId; }

    public Long getId() { return this.id; }
}

