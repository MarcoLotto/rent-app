package app.domain.rental;

import java.util.List;

public enum RentalStatus {
    NOT_YET_COLLECTED ("not_yet_collected"),
    COLLECTED("collected"),
    RETURNED("returned"),
    DUE("due"),
    CLOSED ("closed");

    RentalStatus(String id){
        this.id=id;
    }
    private final String id;

    public String toString() {
        return this.id;
    }

    public static RentalStatus fromId(String id){
        return RentalStatus.valueOf(id.toUpperCase());
    }
}
