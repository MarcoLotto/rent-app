package app.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "user_access_token")
public class UserAccessToken {
	
	@Id
    @Column(name="user_id", nullable=false)
	private Long userId;
	
	@Column(name = "token_value", nullable=false)
	private String tokenValue;
	
	@Column(name = "token_expiration", nullable=false)
	private Date tokenExpiration;

	
	public UserAccessToken(){
		
	}
	
	public UserAccessToken(Long userId){
		this.userId = userId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getTokenValue() {
		return tokenValue;
	}

	public void setTokenValue(String tokenValue) {
		this.tokenValue = tokenValue;
	}

	public Date getTokenExpiration() {
		return tokenExpiration;
	}

	public void setTokenExpiration(Date tokenExpiration) {
		this.tokenExpiration = tokenExpiration;
	}
}
