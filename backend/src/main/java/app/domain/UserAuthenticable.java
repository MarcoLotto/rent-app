package app.domain;

public interface UserAuthenticable {

	public Long getUserId();
	
	public UserAccessToken getAccessToken();
	
	public void setAccessToken(UserAccessToken accessToken);
}
