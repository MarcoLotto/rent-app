package app.domain;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.springframework.data.geo.Point;
import app.enums.ItemStatus;


@Entity
@Table(name = "item")
public class Item {
	
	public Item(){
		this.dateCreated = new Date();
		this.lastUpdated = new Date();
		this.status = ItemStatus.AVAILABLE;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@ManyToOne
    @JoinColumn(name="owner_id", nullable=false)
	private User owner;
	
	@ManyToOne
    @JoinColumn(name="category_id", nullable=false)
	private Category category;

	@ManyToOne
	@JoinColumn(name="price_time_unit", nullable=false)
	private TimeUnit priceTimeUnit;
	
	@Column(name = "title", nullable=false)
	private String title;
	
	@Column(name = "description", nullable=false)
	private String description;
	
	@Column(name="geo_lat", nullable=false)
	private Double geoLat;
	
	@Column(name="geo_lon", nullable=false)
	private Double geoLon;

	@Column(name="price", nullable = false)
	private Float price;

	@Column(name="date_created", nullable=false)
	private Date dateCreated;
	
	@Column(name="last_updated", nullable=false)
	private Date lastUpdated;

	@Column(name="status", nullable = false)
	private ItemStatus status;
	
	public ItemStatus getStatus() { return this.status; }

	public void setStatus(ItemStatus status) { this.status = status; }

	@OneToMany (fetch = FetchType.LAZY, mappedBy = "item", cascade = CascadeType.ALL)
	private Set<ItemPicture> pictures;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User user) {
		this.owner = user;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Double getGeoLat() {
		return geoLat;
	}

	public void setGeoLat(Double geoLat) {
		this.geoLat = geoLat;
	}

	public Double getGeoLon() {
		return geoLon;
	}

	public void setGeoLon(Double geoLon) {
		this.geoLon = geoLon;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public TimeUnit getPriceTimeUnit() {
		return priceTimeUnit;
	}

	public void setPriceTimeUnit(TimeUnit priceTimeUnit) {
		this.priceTimeUnit = priceTimeUnit;
	}

    public Set<ItemPicture> getPictures() {
        return pictures;
    }

    public void setPictures(Set<ItemPicture> pictures) {
        this.pictures = pictures;
    }

    public ItemDTO convertToDTO() {
		ItemDTO dto = new ItemDTO();
		dto.setCategoryId(this.getCategory().getId());
		dto.setTitle(this.getTitle());
		dto.setId(this.getId());
		dto.setLocation(new Point(this.getGeoLat(), this.getGeoLon()));
		return dto;
	}
}
