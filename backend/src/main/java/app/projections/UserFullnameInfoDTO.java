package app.projections;

public interface UserFullnameInfoDTO extends IdEntityDTO {

	public String getName();
	public String getSurename();
}
