package app.projections;

public interface IdEntityDTO {

	public Long getId();
}
