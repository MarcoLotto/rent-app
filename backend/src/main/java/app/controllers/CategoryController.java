package app.controllers;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import app.domain.Category;
import app.services.CategoryService;

@RestController
@RequestMapping(value="/api/v1/categories")
public class CategoryController {

	@Autowired
	private CategoryService service;
	
	@RequestMapping(method=RequestMethod.GET)
    public Map<String, Object> getAllCategories() {
		Map<String, Object> response = new HashMap<>();
        response.put("categories", this.service.getAllCategories());
        return response;
    }
	
	@RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<Category> createCategory(@Valid @RequestBody Category body) {
    	Category category = this.service.saveCategory(body);
    	return new ResponseEntity<Category>(category, HttpStatus.CREATED);
    }
}
