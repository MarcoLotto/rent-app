package app.controllers;

import app.domain.TimeUnit;
import app.services.TimeUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value="/api/v1/timeunit")
public class TimeUnitController {

	@Autowired
	private TimeUnitService service;
	
	@RequestMapping(method=RequestMethod.GET)
    public Map<String, Object> getAllCategories() {
		Map<String, Object> response = new HashMap<>();
        response.put("time_units", this.service.getAllTimeUnits());
        return response;
    }
	
	@RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<TimeUnit> createCategory(@Valid @RequestBody TimeUnit body) {
    	TimeUnit createdResource = this.service.saveTimeUnit(body);
    	return new ResponseEntity<>(createdResource, HttpStatus.CREATED);
    }
}
