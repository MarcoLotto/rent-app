package app.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import app.domain.User;
import app.exceptions.BusinessException;
import app.security.AuthenticationFilter;
import app.services.UserService;

@RestController
@RequestMapping(value="/api/v1/user")
public class UserController {
	
	private static final int USERNAME_MIN_SIZE = 6;
	private static final int USERNAME_MAX_SIZE = 35;
	private static final int PASSWORD_MIN_SIZE = 8;
	private static final int PASSWORD_MAX_SIZE = 30;
	
	@Autowired
	UserService service;
	
	/**
	 * Registra un usuario
	 */
	@RequestMapping(value="/registry", method=RequestMethod.POST)
    public ResponseEntity<User> userRegistration(@Valid @RequestBody User body, 
    		@RequestHeader(AuthenticationFilter.X_AUTH_USERNAME_PARAM) String username, 
    		@RequestHeader(AuthenticationFilter.X_AUTH_PASSWORD_PARAM) String password,
    		HttpServletResponse response) {
		
		this.validateAuthenticationHeaders(username, password);
    	Map<String, Object> responseData = this.service.registerUser(body, username, password);
    	
    	// Seteamos en la respuesta el token de acceso en los headers y en el body la info del usuario
    	response.setHeader(AuthenticationFilter.X_AUTH_TOKEN_PARAM, (String) responseData.get(UserService.TOKEN_PARAM));
    	return new ResponseEntity<User>((User) responseData.get(UserService.USER_PARAM), HttpStatus.CREATED);
    }

	/**
	 * Busca un usuario por id en la DB
	 */
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public User getUser(@PathVariable("id") Long id) {
        return this.service.getUser(id);
    }
    
    /**
	 * Devueve los datos del usuario logueado en esta sesion
	 */
    @RequestMapping(value="/current", method=RequestMethod.GET)
    public User getUser() {
        return this.service.getCurrentUser();
    }
    
    /**
	 * Desloguea el usuario actual
	 */
	@RequestMapping(value="/logout", method=RequestMethod.POST)
    public ResponseEntity<String> userRegistration() {
		this.service.currentUserLogout();
    	return new ResponseEntity<String>("{\"response\": \"Logout successfull\"}", HttpStatus.OK);
    }
    
    private void validateAuthenticationHeaders(String username, String password) {
		if(username.length() < USERNAME_MIN_SIZE || username.length() > USERNAME_MAX_SIZE){
			throw new BusinessException("Username length must be between " + USERNAME_MIN_SIZE + " and " + USERNAME_MAX_SIZE + " characters");
		}
		if(password.length() < PASSWORD_MIN_SIZE || password.length() > PASSWORD_MAX_SIZE){
			throw new BusinessException("Password length must be between " + PASSWORD_MIN_SIZE + " and " + PASSWORD_MAX_SIZE + " characters");
		}
	}
}