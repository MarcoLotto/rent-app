package app.controllers.rental;

import app.domain.Item;
import app.domain.rental.Rental;
import app.exceptions.BadRequestException;
import app.services.rental.RentalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value="/api/v1/rental")
public class RentalController {

    @Autowired
    RentalService rentalService;
    private final String RENTAL_SAVE_SUCCESSFUL= "Rental created successfully";

    @RequestMapping(
            method= RequestMethod.POST
    )
    public ResponseEntity<Map> createRental(@RequestBody Rental body) {

        this.validateRental(body);
        Rental rental = this.rentalService.save(body);
        return new ResponseEntity<Map>(this.buildSaveResponseMessage(rental), HttpStatus.CREATED);
    }

    @RequestMapping(
            value="/{id}",
            method=RequestMethod.GET
    )
    public Rental getRental(@PathVariable("id") Long id) {
        return this.rentalService.getRental(id);
    }

    void validateRental(Rental body){
        if (body.getItemId() == null) {
            throw new BadRequestException("Parameter item_id is mandatory.");
        }
    }

    private Map<String, Object> buildSaveResponseMessage(Rental rental){
        Map<String, Object> response = new HashMap<>();
        response.put("id", rental.getId());
        response.put("message", RENTAL_SAVE_SUCCESSFUL);
        return response;
    }
}
