package app.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import app.domain.ItemDTO;
import app.domain.User;
import app.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import app.domain.Item;
import app.domain.ItemElastic;
import app.exceptions.BadRequestException;
import app.services.ItemService;

@RestController
@RequestMapping(value="/api/v1/item")
public class ItemController {
	
	private static final String ITEM_SAVE_SUCCESSFUL = "Item has been created successfully";

	@Autowired
	ItemService service;

	@Autowired
    UserService userService;
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Item getItem(@PathVariable("id") Long id) {
        return this.service.getItem(id);
    }
	
	@RequestMapping(method=RequestMethod.POST)
    public ResponseEntity<Map> createItem(@RequestBody Item body) {
		this.validateItem(body);
		body.getPictures().forEach((p)-> p.setItem(body));
    	Item item = this.service.saveItem(body);
    	return new ResponseEntity<Map>(this.buildSaveResponseMessage(item), HttpStatus.CREATED);
    }


    @RequestMapping(method = RequestMethod.GET)
    public Page<ItemDTO> searchUsersItems(@RequestParam("offset") int offset, @RequestParam("limit") int limit) {
	    User currentUser = this.userService.getCurrentUser();
        Page<Item> itemsPage = this.service.findItemsByUser(currentUser,offset, limit);
        List<Item> items = itemsPage.getContent();
        List<ItemDTO> itemsDto;
        itemsDto = items.stream().map(item -> item.convertToDTO()).collect(Collectors.toList());
        Page<ItemDTO> responsePage = new PageImpl<ItemDTO>(itemsDto, itemsPage.nextPageable(), itemsPage.getTotalElements());
        return responsePage;
    }
	 /**
     * Busca items por nombre en el elastic
     */
    @RequestMapping(value="/search/title", method=RequestMethod.GET)
    public Page<ItemElastic> searchItemByTitle(@RequestParam("title") String title) {
    	return this.buildResponsePage(this.service.findItemsByTitle(title));
    }
    
    /**
     * Busca items por centro y distancia en el elastic
     */
    @RequestMapping(value="/search/location", method=RequestMethod.GET)
    public Page<ItemElastic> searchItemByLocation(@RequestParam("lat") double centerLat,
    		@RequestParam("lon") double centerLon, @RequestParam("distance") float distanceInKm) {
        Page<ItemElastic> response = this.service.findItemsByLocation(centerLat, centerLon, distanceInKm);
        return this.buildResponsePage(response);
    }

    private Page<ItemElastic> buildResponsePage(Page<ItemElastic> rawPage){
    	Page<ItemElastic> response = new PageImpl<>(rawPage.getContent(), rawPage.nextPageable(), rawPage.getTotalElements());
    	return response;
    }
	
	private void validateItem(Item body) {
		if(body.getTitle() == null || body.getCategory() == null || body.getDescription() == null){
			throw new BadRequestException("The parameters title, description and category are obligatory");
		}
		if(body.getGeoLat() == null || body.getGeoLon() == null){
			throw new BadRequestException("The parameters geoLat and geoLon are obligatory");
		}
	}

	private Map<String, Object> buildSaveResponseMessage(Item item){
		Map<String, Object> response = new HashMap<>();
		response.put("id", item.getId());
		response.put("message", ITEM_SAVE_SUCCESSFUL);
		return response;
	}
}
