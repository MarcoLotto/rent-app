package app.enums;

public enum ItemStatus {
    BOOKED("booked"),
    DUE("due"),
    AVAILABLE("available");

    private String text;

    ItemStatus(String status){
        this.text = status;
    }

    public String toString(){
        return this.text;
    }
}
