package app.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import app.domain.LocalUserAuthentication;

@Repository
public interface LocalUserAuthenticationRepository extends JpaRepository<LocalUserAuthentication, Long> {

	public LocalUserAuthentication findOneByUserId(Long userId);
	
	public LocalUserAuthentication findOneByUsername(String userId);
}
