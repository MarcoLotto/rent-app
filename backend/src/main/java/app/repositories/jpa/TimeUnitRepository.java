package app.repositories.jpa;

import app.domain.TimeUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TimeUnitRepository extends JpaRepository<TimeUnit, Long> {

	public List<TimeUnit> findAll();
}
