package app.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import app.domain.ExternalUserAuthentication;

@Repository
public interface ExternalUserAuthenticationRepository extends JpaRepository<ExternalUserAuthentication, Long> {

	public ExternalUserAuthentication findOneByUserId(Long userId);
	
	public ExternalUserAuthentication findOneByExternalIdAndClient(Long externalId, String client);
}
