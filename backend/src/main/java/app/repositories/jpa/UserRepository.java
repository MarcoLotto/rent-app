package app.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import app.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	public User findOneById(Long id);
}
