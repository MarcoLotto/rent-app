package app.repositories.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import app.domain.UserAccessToken;

@Repository
public interface UserAccessTokenRepository extends JpaRepository<UserAccessToken, Long> {

	public UserAccessToken findOneByUserId(Long userId);
	public UserAccessToken findOneByTokenValue(String tokenValue);
}
