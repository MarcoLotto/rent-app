package app.repositories.jpa;

import app.domain.ItemDTO;
import app.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import app.domain.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {

    Page<Item> findAllByOwner(User owner, Pageable page);

    Item findById(Long itemId);
}
