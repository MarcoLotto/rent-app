package app.repositories.elasticsearch;

import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Point;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import app.domain.ItemElastic;

public interface ItemElasticRepository extends ElasticsearchRepository<ItemElastic, Long> {

    Page<ItemElastic> findAllByTitleLike(String name, Pageable pageable);
    
    Page<ItemElastic> findByLocationWithin(Point geoPoint, Distance distance, Pageable pageable);

    ItemElastic findById(Long id);
}
