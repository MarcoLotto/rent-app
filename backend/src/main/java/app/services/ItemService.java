package app.services;

import app.domain.ItemDTO;
import app.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.stereotype.Service;

import app.domain.Item;
import app.domain.ItemElastic;
import app.exceptions.ResourceSaveException;
import app.repositories.elasticsearch.ItemElasticRepository;
import app.repositories.jpa.ItemRepository;

@Service
public class ItemService {
	
	Logger log = LoggerFactory.getLogger(this.getClass());

	private ItemRepository repository;
	private UserService userService;
	private ItemElasticRepository itemElasticRepository;
	
	@Autowired
	public ItemService(ItemRepository repository, UserService userService, ItemElasticRepository itemElasticRepository){
		this.repository = repository;
		this.userService = userService;
		this.itemElasticRepository = itemElasticRepository;
	}
	
	public Item saveItem(Item item) {
		try{
			// Seteamos el cliente en base al usuario actual
			item.setOwner(this.userService.getCurrentUser());
			
			// Guardamos el item
			Item savedItem = this.repository.save(item);
			
			// Indexamos en el elastic
			this.itemElasticRepository.save(new ItemElastic(savedItem));
			
			return savedItem;
		}
		catch(Exception e){
			log.error(e.toString());
			throw new ResourceSaveException("Error al guardar el item");
		}
	}

	public Item getItem(Long id) {
		return this.repository.findOne(id);
	}
	
	/**
	 * Busca items por su titulo en el elastic
	 */
	public Page<ItemElastic> findItemsByTitle(String title){
		log.info("Searching on Elasticsearch items with title: " + title);
		
		// TODO: Cambiar los limites del page request para paginar
		return this.itemElasticRepository.findAllByTitleLike(title, new PageRequest(0, 10));
	}
	
	/**
	 * Busca en el elasticsearch un item por su locacion
	 */
	public Page<ItemElastic> findItemsByLocation(double lat, double lon, double distanceInKm) {
		log.info("Searching on Elasticsearch items by location Lat: " + lat + " - Lon: " + lon + " - Dist: " + distanceInKm);
		
		// TODO: Cambiar los limites del page request para paginar
		return this.itemElasticRepository.findByLocationWithin(new Point(lat, lon),
				new Distance(distanceInKm, Metrics.KILOMETERS), new PageRequest(0, 10));
	}

	public Page<Item> findItemsByUser(User user, int offset, int limit) {
		Pageable page = new PageRequest(offset, limit);
		return this.repository.findAllByOwner(user, page);
	}

	public Item findItemById(Long itemId){
		return this.repository.findById(itemId);
	}
}
