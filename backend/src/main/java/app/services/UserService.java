package app.services;

import org.springframework.security.core.context.SecurityContextHolder;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.domain.LocalUserAuthentication;
import app.domain.User;
import app.exceptions.ResourceSaveException;
import app.repositories.jpa.UserRepository;
import app.security.AuthenticationService;

@Service
public class UserService {
	
	public static final String USER_PARAM = "user";
	public static final String TOKEN_PARAM = "token";

	private Logger log = LoggerFactory.getLogger(this.getClass());
	
	private UserRepository userRepository;
	private AuthenticationService authenticationService;

	@Autowired
	public UserService(UserRepository userRepository, AuthenticationService authenticationService) {
		this.userRepository = userRepository;
		this.authenticationService = authenticationService;
	}

	/**
	 * Devuelve un user por id
	 */
	public User getUser(Long id) {
		return this.userRepository.findOneById(id);
	}
	
	/**
	 * Devuelve el usuario logueado en esta sesion
	 */
	public User getCurrentUser() {
		Long userId = (Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return this.getUser(userId);
	}
	
	/**
	 * Registra un nuevo usuario
	 */
	public Map<String, Object> registerUser(User user, String username, String password) {
		try {
			// Registramos el usuario (guardamos en DB)
			LocalUserAuthentication authentication = this.authenticationService.registerLocalUser(username, password, user);
			
			log.info("User " + username + " has been created sucessfully");
			
			// Retornamos la info del user y el token
			return this.getRegistrationResponse(user, authentication);
			
		} catch (Exception e) {
			log.error(e.toString());
			throw new ResourceSaveException("Error creating user");
		}
	}
	
	private Map<String, Object> getRegistrationResponse(User user, LocalUserAuthentication authentication){
		Map<String, Object> response = new HashMap<>();
		response.put(USER_PARAM, user);
		response.put(TOKEN_PARAM, authentication.getAccessToken().getTokenValue());
		return response;
	}

	/**
	 * Desloguea al usuario logueado en la sesion actual
	 */
	public void currentUserLogout() {
		User currentUser = this.getCurrentUser();
		this.authenticationService.userLogout(currentUser);
	}
}
