package app.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.domain.Category;
import app.repositories.jpa.CategoryRepository;

@Service
public class CategoryService {
	
	@Autowired
	CategoryRepository repository;

	public List<Category> getAllCategories() {
		return this.repository.findAll();
	}

	public Category saveCategory(Category category) {
		return this.repository.save(category);
	}
}
