package app.services;

import app.domain.Category;
import app.domain.TimeUnit;
import app.repositories.jpa.TimeUnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TimeUnitService {
	
	@Autowired
	private TimeUnitRepository repository;

	public List<TimeUnit> getAllTimeUnits() {
		return this.repository.findAll();
	}

	public TimeUnit saveTimeUnit(TimeUnit timeUnit) {
		return this.repository.save(timeUnit);
	}
}
