package app.services.rental;

import app.domain.rental.Rental;
import app.exceptions.BadRequestException;
import app.exceptions.ResourceSaveException;
import app.repositories.jpa.RentalRepository;
import app.repositories.jpa.UserRepository;
import app.security.AuthenticationService;
import app.services.ItemService;
import app.services.UserService;
import app.domain.Item;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;

@Service
public class RentalService {
    Logger log = LoggerFactory.getLogger(RentalService.class);

    private RentalRepository rentalRepository;
    @Autowired
    UserService userService;
    @Autowired
    ItemService itemService;

    @Autowired
    public RentalService(RentalRepository rentalRepository, ItemService itemService, UserService userService) {
        this.userService = userService;
        this.rentalRepository = rentalRepository;
        this.itemService = itemService;
    }

    public Rental save(Rental rental) {
        try {
            Item item = this.itemService.findItemById(rental.getItemId());
            if (item == null){
                throw new BadRequestException("Item not found.");
            }
            rental.setBorrower(this.userService.getCurrentUser());
            rental.setRentalDate(java.sql.Date.valueOf(LocalDate.now()));
            rental.setReturnDate(java.sql.Date.valueOf(LocalDate.now().plusDays(30)));
            rentalRepository.save(rental);
            return rental;
        } catch (Exception e) {
            this.log.error(e.toString());
            throw new ResourceSaveException("Error al guardar el rental.");
        }
    }

    public Rental getRental(Long id) {
        return this.rentalRepository.findOne(id);
    }
}
