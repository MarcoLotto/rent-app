package app.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import app.exceptions.BusinessException;

/**
 * Cada instancia (server) puede tener un scope en donde funcionaran determindas caracteristicas
 * de la aplicacion
 */
@Component
public class InstanceScope {

	public static final String DEVELOPMENT_SCOPE = "development";
	public static final String CONSUMER_SCOPE = "consumers";
	
	public static final String SCOPE_ENV_VARIABLE = "SCOPE";
	
	private String scope;
	
	@Autowired
	public InstanceScope(Environment env) {
		this.scope = env.getProperty(SCOPE_ENV_VARIABLE);
	}
	
	public String getCurrentScope(){
		return this.scope;
	}
	
	public boolean isCurrentScope(String expectedScope){
		if(this.scope == null){
			throw new BusinessException("No SCOPE has been defined in env for this instance");
		}
		return this.scope.equals(expectedScope);
	}
}
