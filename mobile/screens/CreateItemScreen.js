import React from 'react';
import { Image, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View, 
        Button, Alert, Dimensions, TextInput, Picker} from 'react-native';
import * as actions from '../actions';        

export default class CreateItemScreen extends React.Component {
  
  static navigationOptions = {
    header: null,
  }; 

  constructor(props){
    super(props);
    this.state={}
  }

  render() {
    return (
      <View style={[styles.container]}>
        <ScrollView
          style={[styles.container2]}>
        
          <Text style={[styles.inputDetail]}>Titulo</Text>
          <TextInput
            style={[styles.input]}
            placeholder="Ingrese el titulo de la publicacion"
            onChangeText={(title) => this.setState({title})}
            value={this.state.title}
          />

          <Text style={[styles.inputDetail]}>Categoria</Text>
          <Picker style={[styles.pickerContainer]}
            
            selectedValue={this.state.category}
            onValueChange={(itemValue, itemIndex) => this.setState({category: itemValue})}>
            <Picker.Item label="Indumentaria" value="2" />
            <Picker.Item label="Electronica" value="3" />
            <Picker.Item label="Hogar" value="4" />
            <Picker.Item label="Fiestas" value="9" />
            <Picker.Item label="Otros" value="39" />
          </Picker>

          <Text style={[styles.inputDetail]}>Descripcion</Text>
          <TextInput
            style={[styles.input]}
            numberOfLines = {4}
            placeholder="Ingrese la descripcion"
            onChangeText={(description) => this.setState({description})}
            value={this.state.description}
          />

          <Text style={[styles.inputDetail]}>Precio</Text>
          <TextInput
            style={[styles.input]}
            placeholder="Ingrese el precio del alquiler por dia"
            onChangeText={(price) => this.setState({price})}
            value={this.state.price}
          />

          <Button title="Create Item"
            onPress={this.createItem.bind(this)}
          />
        </ScrollView>

      </View>
    );
  }

  createItem(){
    Alert.alert("Alquiler creado");
  }
  
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ecf0f1',
    flex: 1,
    flexDirection: 'column',
  },
  container2: {
    marginTop: 25
    
  },
  input: {
    margin: 20,
    //height: 40 
    fontSize: 16,

  },
  inputDetail: {
    marginTop: 5,
    marginBottom: 0,
    marginLeft: 20,
    fontSize: 24,
    color: 'dimgray'
  },
  pickerContainer:{
    marginTop: 0,
    paddingTop:0
  },
});
