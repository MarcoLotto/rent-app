import React from 'react';
import { Image, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View, 
  Button, Alert, Dimensions} from 'react-native';
  import { connect } from 'react-redux';
 import * as actions from '../actions';

  import { VIP } from '../components';

class ItemDescription extends React.Component {
  
    static navigationOptions = ({navigation}) => ({
      title: `Item ${navigation.state.params.item.title}`,
      id: `Item ${navigation.state.params.item.id}`
    }); 



    constructor(props){
      super(props);
      this.state = {
        item: {}
      }
    }

    componentWillReceiveProps(nextProps){
      this.setState({
        item : nextProps.item.item
      })
    }

    _confirmRent(){
      Alert.alert("Felicidades, alquilaste!");
        //this.props.navigation.navigate("ItemRent", {title: "Alquilar" + this.state.item.title })
    }
    render() {
      return (


        <View style={styles.container}>
 
        <VIP description={this.state.item.description} price={this.state.item.price || 150.00} timeUnit={this.state.item.priceUnit || 15} title={this.state.item.title} imgSrc="https://media.aws.alkosto.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/m/h/mhc-v11_angle02-mid.jpg"/>
        

       <Button onPress={() => this._confirmRent()} title = "Alquilar">

       

        </Button>

        </View>
        );
    }
  }

  const styles = StyleSheet.create({
    container: {
      backgroundColor: '#ecf0f1',
      marginTop: 20,
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center',
      alignItems: 'center',
    }
  });
function mapStateToProps (state) {
  return {
   items: state.items,
   item: state.item
  }
 }

 function mapDispatchToProps (dispatch) {
  return {
   fetchItems: () => dispatch(actions.fetchItems()),
   fetchItem: (itemId) => dispatch(actions.fetchItem(itemId)),
   rent: (item) => dispath(actions.rentItem(item))
  }
 }

 export default connect(
  mapStateToProps,
  mapDispatchToProps
  )(ItemDescription)