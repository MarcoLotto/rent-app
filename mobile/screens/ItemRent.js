import React from 'react';
import { Image, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View, 
        Button, Alert, Dimensions} from 'react-native';

import { RentalConfirm } from '../components/';
 import { connect } from 'react-redux';
 import * as actions from '../actions';


class ItemRent extends React.Component {
  
  static navigationOptions = ({navigation}) => ({
    title: 'Item' + navigation.state.params.title
  }); 


  render() {
    return (
      <View style={styles.container}>

      
                <View style={[styles.congratsContainer]}>
                  
                    <Text style={[styles.congratsText]}> Felicidades alquilaste {this.props.item.title}</Text>
                    <Text style={[styles.contacGiverText]}> Ponte en contacto con Jorge para obtener el producto.</Text>
                </View> 
              

      </View>

    );
  }
}


const styles = StyleSheet.create({
  container: {
    backgroundColor: '#ecf0f1',
    marginTop: 20,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  }
});

function mapStateToProps (state) {
  return {
   items: state.items,
   item: state.item
  }
 }

 function mapDispatchToProps (dispatch) {
  return {
   fetchItems: () => dispatch(actions.fetchItems()),
   fetchItem: (itemId) => dispatch(actions.fetchItem(itemId)),
   rent: (item) => dispath(actions.rent(item))
  }
 }

 export default connect(
  mapStateToProps,
  mapDispatchToProps
  )(ItemRent)