

import {FETCHING_ITEM_SUCCESS, FETCHING_ITEM_FAILURE, FETCHING_ITEMS, FETCHING_DATA_SUCCESS, FETCHING_DATA_FAILURE } from './constants'
import getItemsFromAPI from './api'

const FIXED_USER_TOKEN = '6ea7f174-3d06-49bb-a1cb-cffa8a341179';

export function getItems() {
  return {
    type: FETCHING_ITEMS
  }
}

export function getItemsSuccess(data) {
  return {
    type: FETCHING_DATA_SUCCESS,
    items: data,
  }
}

export function getItemsFailure() {
  return {
    type: FETCHING_DATA_FAILURE
  }
}


export function getItem() {
  return {
    type: FETCHING_ITEMS
  }
}

export function getItemSuccess(data) {
  return {
    type: FETCHING_ITEM_SUCCESS,
    item: data,
  }
}

export function getItemFailure() {
  return {
    type: FETCHING_ITEM_FAILURE
  }
}

export function fetchItem(itemId) {
  return (dispatch) => {
   return fetch('http://ec2-54-227-102-108.compute-1.amazonaws.com:8092/api/v1/item/'+itemId.id,{
    method: 'GET',
    headers: {
      'X-Auth-Token': '6ea7f174-3d06-49bb-a1cb-cffa8a341179',
      'Content-Type': 'application/json',
    }}).then((data) => {
      return data.json()

    }).then(function(elem){
      dispatch(getItemSuccess(elem))
    }
    ).catch((err) => console.log('err:', err))
  }
}

export function fetchItems() {
  return (dispatch) => {
   return fetch('http://ec2-54-227-102-108.compute-1.amazonaws.com:8092/api/v1/item/search/title?title=',{
    method: 'GET',
    headers: {
      'X-Auth-Token': FIXED_USER_TOKEN,
      'Content-Type': 'application/json',
    }}).then((data) => {
      return data.json()

    }).then(function(elem){dispatch(getItemsSuccess(elem))}).catch((err) => console.log('err:', err))
  }
}

export function createItem(item) {
  
  
  return (dispatch) => {
   return fetch('http://ec2-54-227-102-108.compute-1.amazonaws.com:8092/api/v1/item',
   {
    method: 'POST',
    headers: {
      'X-Auth-Token': FIXED_USER_TOKEN,
      'Content-Type': 'application/json',
    },
    body: 
    {	
      "category": { "id": item.category },
      "title": item.title,
      "description": item.description,
      "geoLat": -34.5098168,
      "geoLon": -58.4937188,
      "price": parseFloat(item.price),
      "priceTimeUnit": { "id": 2 },
      "pictures": [{"source": "https://www.o2.co.uk/shop/homepage/images/shop15/brand/apple/iphone-6s/apple-iphone-6s-2016-ios-ios-10-gallery-img-5-101016.jpg"}]
    }
  })
  .then((data) => {
      if(data.status==201){
        alert("Item created ok")
      }else{
        alert(data.status)
      }
      
    }).catch((err) => { 
      console.log('err:', err)
    })
  }
}


  export function rentItem(item) {
  return (dispatch) => {
   return fetch('http://ec2-54-227-102-108.compute-1.amazonaws.com:8092/api/v1/rent/create',
   {
    method: 'POST',
    headers: {
      'X-Auth-Token': FIXED_USER_TOKEN,
      'Content-Type': 'application/json',
    },
    body: 
    { 
      "item": { "id": item.id }
    }
  })
  .then((data) => {
      return data.json()
      
    }).then(function(elem){
      return 0
    }).catch((err) => console.log('err:', err))
  }
}