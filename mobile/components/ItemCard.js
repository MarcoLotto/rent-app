import React from 'react';
import { Text, Button, Alert, StyleSheet, View, Image, TouchableHighlight } from 'react-native';
import './ItemCard';

const IMG_HEIGTH = 120;

export class ItemCard extends React.Component {

  cardSelected(e) {
    if(this.props.onSelection){
      this.props.onSelection(this.props);  // TODO: Reemplazar por un id
    }
  }

  renderPrice(){
    if (!this.props.isCategory)
      return <Text style={[styles.price]}>{this.props.price}$ /{this.props.priceTime}</Text>
  }

  render() {
    return (
      <View style={[styles.component]}>
        <TouchableHighlight onPress={this.cardSelected.bind(this)} underlayColor="white" style={[styles.hover]}>
          <View style={[styles.hoverView]} >
            <View style={[styles.imgContainer]}>
              <Image
                resizeMode="contain"
                source={{ uri: this.props.imgSrc }}
                style={[styles.img]}
                />
              </View>
              <View style={[styles.infoContainer]}>
                <Text style={[styles.title]}>{this.props.title}</Text>
                {this.renderPrice()}
              </View> 
          </View>
        </TouchableHighlight>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  component: {
    flex: 1,
    margin: 10
  },

  hover: {
    flex: 1, 
    borderRadius: 3
  },

  hoverView: {
	  backgroundColor: 'white',
    flex: 1, 
    borderRadius: 3,
    position: 'relative',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
  },

  price: {
    marginTop: 5,
    marginHorizontal: 10,
    color: 'rgb(0, 0, 0)',
    fontSize: 16
  },

  title: {
    marginTop: 1,
    marginHorizontal: 10,
    marginBottom: 5,
    color: 'rgb(100, 100, 100)',
    fontSize: 15
  },

  imgContainer: {
    flex: 1,
    height: IMG_HEIGTH
  },

  img: {
    flex: 1,
    borderRadius: 2,
    position: 'relative',
    margin: 2,
    height: undefined, 
    width: undefined
  },

  infoContainer: {
    flexDirection: 'column',
    borderTopWidth: 1,
    borderTopColor: 'rgba(0,0,0,0.2)' 
  }
});
