import React from 'react';
import { Text, Button, Alert } from 'react-native';
import { WebBrowser } from 'expo';
import { Facebook } from 'expo';

export class FacebookLogin extends React.Component {
  render() {
    return (
     
      <Button title="Login with Facebook"
              onPress={this._handleFacebookLogin}
           />

          /*<Text>Hola mundo!</Text>*/
    );
  }

  _handleFacebookLogin = async () => {
    try {
      const { type, token } = await Facebook.logInWithReadPermissionsAsync(
        '378133079287792', // Replace with your own app id in standalone app
        { permissions: ['public_profile'] }
      );

      switch (type) {
        case 'success': {
          // Get the user's name using Facebook's Graph API
          const response = await fetch(`https://graph.facebook.com/me?access_token=${token}`);
          const profile = await response.json();
          Alert.alert('Logged in!', `Hi ${profile.name}!`,);
          break;
        }
        case 'cancel': {
          Alert.alert('Cancelled!', 'Login was cancelled!',);
          break;
        }
        default: {
          Alert.alert('Oops!','Login failed!',);
        }
      }
    } catch (e) {
      Alert.alert('Oops!','Login failed!',console.log(e));
    }
  };
  
}
