import React from 'react';
import { Text, Button, Alert, StyleSheet, View, Image, TouchableHighlight } from 'react-native';
 import { connect } from 'react-redux';
 import * as actions from '../actions';

class RentalConfirm extends React.Component {

		render() {
			return (
				<View style={[styles.component]}>
		            <View style={[styles.congratsContainer]}>
		            	
		                <Text style={[styles.congratsText]}> Felicidades alquilaste {this.props.item.title}</Text>
		                <Text style={[styles.contacGiverText]}> Ponte en contacto con Jorge para obtener el producto.</Text>
		            </View> 
            	</View>
			);
		}
}


var styles = StyleSheet.create({

	congratsContainer: {
	 	flexDirection: 'column',
    	borderTopWidth: 1,
    	borderTopColor: 'rgba(0,0,0,0.2)' 
	},
	congratsText: {
	    marginTop: 5,
	    marginHorizontal: 10,
	    marginBottom: 5,
	    color: 'rgb(100, 100, 100)',
	    fontSize: 32
  	},
  	contacGiverText: {
	    marginTop: 5,
	    marginHorizontal: 10,
	    marginBottom: 5,
	    color: 'rgb(100, 100, 100)',
	    fontSize: 22
  	}
  }
);

function mapStateToProps (state) {
  return {
   items: state.items,
   item: state.item
  }
 }

 function mapDispatchToProps (dispatch) {
  return {
   fetchItems: () => dispatch(actions.fetchItems()),
   fetchItem: (itemId) => dispatch(actions.fetchItem(itemId)),
   rent: (item) => dispath(actions.rent(item))
  }
 }

 export default connect(
  mapStateToProps,
  mapDispatchToProps
  )(RentalConfirm)