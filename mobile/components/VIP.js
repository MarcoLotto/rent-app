import React from 'react';
import { Text, Button, Alert, StyleSheet, View, Image, TouchableHighlight } from 'react-native';
//import {Button} from 'react-native-elements';
const IMG_HEIGTH = 190;

export class VIP extends React.Component {

	onPressRentIt(){

	}

	onPressGoBack(){

	}

	render() {
		return (
			<View style={[styles.component]}>
				<View style={[styles.imgContainer]}>
	              <Image
	                resizeMode="cover"
	                source={{ uri: this.props.imgSrc }}
	                style={[styles.img]}
	                />
	            </View>
	            <View style={[styles.infoContainer]}>
	                <Text style={[styles.title]}>{this.props.title}</Text>
	            </View> 
	            <View style={[styles.infoPriceContainer]}>
	            	<Text style={[styles.price]}> Precio: </Text>
	                <Text style={[styles.price]}> ${this.props.price} por {this.props.timeUnit}</Text>
	            </View> 
	            <View style={[styles.descrContainer]}>
	                <Text style={[styles.descr]}> {this.props.description}</Text>
	            </View> 
	            
            </View>
		);
	}
}


var styles = StyleSheet.create({
  component: {
    flex: 1,
    margin: 10,
    
  },

  hover: {
    flex: 1, 
    borderRadius: 3
  },

  hoverView: {
	  backgroundColor: 'white',
    flex: 1, 
    borderRadius: 3,
    position: 'relative',
    borderStyle: 'solid',
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,0.2)',
  },

  price: {
    marginTop: 5,
    marginHorizontal: 10,
    color: 'rgb(0, 0, 0)',
    fontSize: 22
  },


  descr: {
    marginTop: 15,
    marginHorizontal: 10,
    color: 'dimgray',
    fontSize: 18
  },

  title: {
    marginTop: 5,
    marginHorizontal: 10,
    marginBottom: 5,
    color: 'rgb(100, 100, 100)',
    fontSize: 25
  },

  imgContainer: {
    flex: 1,
    height: IMG_HEIGTH
  },

  img: {
    flex: 1,
    borderRadius: 2,
    position: 'relative',
    margin: 2,
    height: undefined, 
    width: undefined
  },

  infoContainer: {
    flexDirection: 'column',
    borderTopWidth: 1,
    borderTopColor: 'rgba(0,0,0,0.2)' 
  },

  infoPriceContainer: {
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: 'rgba(0,0,0,0.2)' 
  },

  infoDescrContainer: {
    flexDirection: 'column',
    borderTopWidth: 1,
    borderTopColor: 'rgba(0,0,0,0.2)' 
  },

  
});
