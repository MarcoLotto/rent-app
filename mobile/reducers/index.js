import { combineReducers } from 'redux';
import categories from './categories';
import items from './items';
import item from './item';

const rootReducer = combineReducers({
    categories,
    items,
    item
})

export default rootReducer