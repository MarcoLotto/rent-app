 import {FETCHING_ITEM_SUCCESS, FETCHING_ITEM_FAILURE, FETCHING_ITEMS, FETCHING_DATA_SUCCESS, FETCHING_DATA_FAILURE } from '../constants'
const initialState = {
  item: null,
  dataFetched: false,
  isFetching: false,
  error: false
}

export default function item (state = initialState, action) {
  switch (action.type) {
    case FETCHING_ITEMS:
      return {
        ...state,
        item: null,
        isFetching: true
      }
    case FETCHING_ITEM_SUCCESS:
      return {
        ...state,
        isFetching: false,
        item: action.item
      }
    case FETCHING_ITEM_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      }
    default:
      return state
  }
}