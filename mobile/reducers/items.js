import { FETCHING_ITEMS, FETCHING_DATA_SUCCESS, FETCHING_DATA_FAILURE } from '../constants'
const initialState = {
  items: [],
  dataFetched: false,
  isFetching: false,
  error: false
}

export default function items (state = initialState, action) {
  switch (action.type) {
    case FETCHING_ITEMS:
      return {
        ...state,
        items: [],
        isFetching: true
      }
    case FETCHING_DATA_SUCCESS:
      return {
        ...state,
        isFetching: false,
        items: action.items
      }
    case FETCHING_DATA_FAILURE:
      return {
        ...state,
        isFetching: false,
        error: true
      }
    default:
      return state
  }
}